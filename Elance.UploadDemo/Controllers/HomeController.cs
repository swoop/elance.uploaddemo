﻿using System;
using System.Web.Mvc;

namespace Elance.UploadDemo.Controllers
{
    using System.IO;
    using System.Threading;

    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Message = "Welcome to ASP.NET MVC!";

            return View();
        }

        public ActionResult About()
        {
            return View();
        }

        public JsonResult Upload()
        {
            var httpRequest = this.Request;
            if (httpRequest.Files.Count > 0)
            {
                // JUST FOR SIMULATING SLOW NETWORK
                Thread.Sleep(2000);
                var postedFile = httpRequest.Files[0];
                if (postedFile != null)
                {
                    var filePath = Path.Combine(this.Server.MapPath("~/App_Data"), postedFile.FileName);
                    postedFile.SaveAs(filePath);
                    return new JsonResult() { Data = new { success = true, fileName = postedFile.FileName } };
                }
            }

            return new JsonResult() { Data = new { success = false } }; 
        }

        public ActionResult Preview(string fileName)
        {
            try
            {
                var stream = new FileStream(Path.Combine(this.Server.MapPath("~/App_Data"), fileName), FileMode.Open);
                return new FileStreamResult(stream, "image/jpeg");
            }
            catch (Exception)
            {
                return new HttpNotFoundResult();
            }
        }
    }
}
