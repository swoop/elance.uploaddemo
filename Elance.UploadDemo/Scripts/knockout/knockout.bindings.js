﻿
; (function (ko) {
    ko.bindingHandlers.fileupload = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var value = ko.utils.unwrapObservable(valueAccessor());
            var url = ko.utils.unwrapObservable(value.url);

            var start = ko.utils.unwrapObservable(value.start) || viewModel.start;
            var progress = ko.utils.unwrapObservable(value.progress) || viewModel.progress;
            var done = ko.utils.unwrapObservable(value.done) || viewModel.done;
            var always = ko.utils.unwrapObservable(value.always) || viewModel.always;
            var submit = ko.utils.unwrapObservable(value.submit) || viewModel.submit;
            var send = ko.utils.unwrapObservable(value.send) || viewModel.send;
            var add = ko.utils.unwrapObservable(value.add) || viewModel.add;

            $(element).fileupload({
                url: url,
                //autoUpload:true,
                dataType: 'json',
                submit: function (e, data) {
                    if (submit)
                        return submit(data);
                },
                start: function (e, data) { if (start) start(data); },
                send: function (e, data) { if (send) send(data); },
                progress: function (e, data) { if (progress) progress(data); },
                done: function (e, data) { if (done) done(data); },
                always: function (e, data) { if (always) always(data); },
                //add: function (e, data) { if (add) add(data); },
                
            });
        }
    };

        
})(ko);